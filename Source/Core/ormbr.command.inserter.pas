{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.inserter;

interface

uses
  DB,
  Rtti,
  Math,
  StrUtils,
  SysUtils,
  TypInfo,
  Variants,
  ormbr.core.consts,
  ormbr.command.abstract,
  ormbr.mapping.classes,
  ormbr.factory.interfaces,
  ormbr.dml.commands,
  ormbr.rtti.helper,
  ormbr.objects.helper,
  ormbr.mapping.rttiutils,
  ormbr.mapping.explorer,
  ormbr.types.blob;

type
  TCommandInserter = class(TDMLCommandAbstract)
  private
    FDMLAutoInc: TDMLCommandAutoInc;
    function GetParamValue(AInstance: TObject; AProperty: TRttiProperty;
      AFieldType: TFieldType): Variant;
  public
    constructor Create(AConnection: IDBConnection; ADriverName: TDriverName;
      AObject: TObject); override;
    destructor Destroy; override;
    function GenerateInsert(AObject: TObject): string;
    function AutoInc: TDMLCommandAutoInc;
    procedure UpdateAutoIncField(AObject: TObject);
  end;

implementation

{ TCommandInserter }

constructor TCommandInserter.Create(AConnection: IDBConnection;
  ADriverName: TDriverName; AObject: TObject);
begin
  inherited Create(AConnection, ADriverName, AObject);
  FDMLAutoInc := TDMLCommandAutoInc.Create;
end;

destructor TCommandInserter.Destroy;
begin
  FDMLAutoInc.Free;
  inherited;
end;

function TCommandInserter.GenerateInsert(AObject: TObject): string;
var
  LColumns: TColumnMappingList;
  LColumn: TColumnMapping;
  LPrimaryKey: TPrimaryKeyMapping;
  LBooleanValue: Integer;
begin
  FCommand := FGeneratorCommand.GeneratorInsert(AObject);
  Result := FCommand;
  FParams.Clear;
  /// <summary>
  /// Alimenta a lista de par�metros do comando Insert com os valores do Objeto.
  /// </summary>
  LColumns := TMappingExplorer.GetInstance.GetMappingColumn(AObject.ClassType);
  if LColumns = nil then
    raise Exception.CreateFmt(cMESSAGECOLUMNNOTFOUND, [AObject.ClassName]);

  LPrimaryKey := TMappingExplorer.GetInstance
                                   .GetMappingPrimaryKey(AObject.ClassType);

  FDMLAutoInc.PrimaryKey := LPrimaryKey;

  FDMLAutoInc.Sequence := TMappingExplorer
                                  .GetInstance
                                    .GetMappingSequence(AObject.ClassType);

  FDMLAutoInc.ExistSequence := (FDMLAutoInc.Sequence <> nil);
  
  for LColumn in LColumns do
  begin
    if LColumn.ColumnProperty.IsNullValue(AObject) then
      Continue;
    if LColumn.IsNoInsert then
      Continue;
    if LColumn.IsJoinColumn then
      Continue;
    /// <summary>
    ///  Verifica se existe PK, pois autoinc s� � usado se existir.
    ///  Caso n�o exista um "Sequence" n�o � possivel buscar o novo ID antes de
    ///  inserir no banco, assim o novo valor s� ser� recuperado pela fun��o
    ///  "UpdateAutoIncField" apos o registo estar gravado no banco
    /// </summary>
    if (LPrimaryKey <> nil) and (FDMLAutoInc.ExistSequence) then
    begin
      if LPrimaryKey.Columns.IndexOf(LColumn.ColumnName) > -1 then
      begin
        if LPrimaryKey.AutoIncrement then
        begin
          LColumn.ColumnProperty.SetValue(AObject,
                                            FGeneratorCommand
                                              .GeneratorAutoIncNextValue(AObject, FDMLAutoInc));
        end;
      end;
    end;
    /// <summary>
    ///   Alimenta cada par�metro com o valor de cada propriedade do objeto.
    /// </summary>
    with FParams.Add as TParam do
    begin
      Name := LColumn.ColumnName;
      DataType := LColumn.FieldType;
      ParamType := ptInput;
      Value := GetParamValue(AObject, LColumn.ColumnProperty, LColumn.FieldType);
      /// <summary>
      ///   Tratamento para o tipo ftBoolean nativo, indo como Integer
      ///   para gravar no banco.
      /// </summary>
      if DataType in [ftBoolean] then
      begin
        LBooleanValue := IfThen(Boolean(Value), 1, 0);
        DataType := ftInteger;
        Value := LBooleanValue;
      end;
    end;
  end;
end;

function TCommandInserter.GetParamValue(AInstance: TObject;
  AProperty: TRttiProperty; AFieldType: TFieldType): Variant;
begin
  Result := Null;
  case AProperty.PropertyType.TypeKind of
    tkEnumeration:
      Result := AProperty.GetEnumToFieldValue(AInstance, AFieldType).AsVariant;
  else
    if AFieldType = ftBlob then
      Result := AProperty.GetNullableValue(AInstance).AsType<TBlob>.ToBytes
    else
      Result := AProperty.GetNullableValue(AInstance).AsVariant;
  end;
end;

procedure TCommandInserter.UpdateAutoIncField(AObject: TObject);
var
  LColumns: TColumnMappingList;
  LColumn: TColumnMapping;
  LPrimaryKey: TPrimaryKeyMapping;
begin
  /// <summary>
  /// Alimenta a lista com os valores do Objeto.
  /// Caso n�o existe "Sequence" ele busca o ID gerado pelo banco de dados
  /// </summary>
  LColumns := TMappingExplorer.GetInstance.GetMappingColumn(AObject.ClassType);
  if LColumns = nil then
    raise Exception.CreateFmt(cMESSAGECOLUMNNOTFOUND, [AObject.ClassName]);

  LPrimaryKey := TMappingExplorer.GetInstance
                                   .GetMappingPrimaryKey(AObject.ClassType);

  FDMLAutoInc.Sequence := TMappingExplorer
                                  .GetInstance
                                    .GetMappingSequence(AObject.ClassType);

  FDMLAutoInc.ExistSequence := (FDMLAutoInc.Sequence <> nil);
  FDMLAutoInc.PrimaryKey := LPrimaryKey;

  if (LPrimaryKey <> nil) and (Not FDMLAutoInc.ExistSequence) then
  begin
    for LColumn in LColumns do
    begin
      if LColumn.ColumnProperty.IsNullValue(AObject) then
        Continue;
      if LColumn.IsNoInsert then
        Continue;
      if LColumn.IsJoinColumn then
        Continue;
      /// <summary>
      /// Verifica se existe PK, pois autoinc s� � usado se existir.
      /// </summary>
      if LPrimaryKey.Columns.IndexOf(LColumn.ColumnName) > -1 then
      begin
        if LPrimaryKey.AutoIncrement then
        begin
          LColumn.ColumnProperty.SetValue(AObject,
                                            FGeneratorCommand
                                              .GeneratorAutoIncCurrentValue(AObject, FDMLAutoInc));
        end;

      end;
    end;
  end;
end;

function TCommandInserter.AutoInc: TDMLCommandAutoInc;
begin
  Result := FDMLAutoInc;
end;

end.
